import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestoreDocument } from "@angular/fire/firestore";
import { PartyData, UserData } from "app/server/data";
import { Firebase } from "app/server/firebase";
import { nullToUndefined } from "app/utils/util";
import { BehaviorSubject, Observable, of } from "rxjs";
import { map, switchMap, tap } from "rxjs/operators";

@Injectable({ providedIn: "root" })
export class State {
	private _userDoc?: AngularFirestoreDocument<UserData>;
	private _userInfo?: firebase.UserInfo;
	private readonly _userData$: Observable<UserData | undefined>;
	private _userData?: UserData;

	private readonly _partyCodeSubject = new BehaviorSubject<string | undefined>(
		undefined,
	);
	private _partyDoc?: AngularFirestoreDocument<PartyData>;
	private readonly _partyData$: Observable<PartyData | undefined>;
	private _partyData?: PartyData;

	constructor(auth: AngularFireAuth, private readonly _firebase: Firebase) {
		this._userData$ = auth.authState.pipe(
			tap((userInfo) => (this._userInfo = nullToUndefined(userInfo))),
			map((userInfo) =>
				userInfo !== null
					? this._firebase.getUserDataDoc(userInfo.uid)
					: undefined,
			),
			tap((userDoc) => (this._userDoc = userDoc)),
			switchMap((userDoc) => _firebase.valueChangesWithoutOwn(userDoc)),
		);
		this._userData$.subscribe((userData) => {
			this._userData = userData;
			this._partyCodeSubject.next(
				userData === undefined ? undefined : userData.partyCode,
			);
		});
		this._partyData$ = this._partyCodeSubject.pipe(
			switchMap((partyCode) => {
				if (partyCode === undefined) return of(undefined);
				this._partyDoc = this._firebase.getPartyDoc(partyCode);
				return this._firebase.valueChangesWithoutOwn(this._partyDoc);
			}),
		);
		this._partyData$.subscribe((partyData) => (this._partyData = partyData));
	}

	get userInfo(): firebase.UserInfo | undefined {
		return this._userInfo;
	}

	get userData$(): Observable<UserData | undefined> {
		return this._userData$;
	}

	get userData(): UserData | undefined {
		return this._userData;
	}

	get partyData(): PartyData | undefined {
		return this._partyData;
	}

	createAndJoinParty(): void {
		void this.joinParty(this._firebase.createParty());
	}

	async tryToJoinParty(partyCode: string): Promise<boolean> {
		if (partyCode.length === 0) return false;
		return this.joinParty(this._firebase.getPartyDoc(partyCode));
	}

	private async joinParty(
		partyDoc: AngularFirestoreDocument<PartyData>,
	): Promise<boolean> {
		this._partyDoc = partyDoc;
		this._partyData = await this._firebase.getData(partyDoc);
		if (this._partyData === undefined) return false;
		this._firebase.joinParty(
			this._userDoc!,
			this._partyDoc,
			this.userInfo!,
			this._userData!,
			this._partyData,
		);
		this._partyCodeSubject.next(partyDoc.ref.id);
		return true;
	}

	abandonParty(): void {
		this._firebase.abandonParty(
			this._userDoc!,
			this._partyDoc!,
			this.userInfo!.uid,
			this.partyData!,
		);
		this._userData!.partyCode = undefined;
		this._partyDoc = undefined;
		this._partyData = undefined;
		this._partyCodeSubject.next(undefined);
	}

	saveUserData(): void {
		void this._userDoc!.set(this._userData!);
	}

	savePartyData(): void {
		void this._partyDoc!.update(this._partyData!);
	}

	get firstPartyParticipant(): boolean {
		return (
			this._partyData!.participants.length > 0 &&
			this.userInfo!.uid === this._partyData!.participants[0].uid
		);
	}
}
