export type Level = "easy" | "medium" | "difficult";

export interface GameData {
	digits?: OneToNineOrNull[];
	gameIndex?: number;
	score?: number;
}

export interface UserData {
	theme?: string;
	gameData?: GameData;
	partyCode?: string;
}

export interface PartyParticipant {
	uid: string;
	displayName: string;
	photoURL: string | null;
}

export interface PartyData {
	// timestamp to be able to delete old parties?
	// gameData?: GameData;
	level: Level;
	participants: PartyParticipant[];
}
