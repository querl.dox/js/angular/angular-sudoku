import { Injectable } from "@angular/core";
import {
	AngularFirestore,
	AngularFirestoreDocument,
} from "@angular/fire/firestore";
import { PartyData, UserData } from "app/server/data";
import { assertDefined } from "app/utils/util";
import * as firebase from "firebase/app";
import { Observable, of } from "rxjs";
import { filter, first, map } from "rxjs/operators";

const SIX_CHARS = 2176782336; // = Math.pow(36, 6) = 6 characters in base 36 notation = about 25 days

@Injectable({ providedIn: "root" })
export class Firebase {
	constructor(private readonly db: AngularFirestore) {}

	createParty(): AngularFirestoreDocument<PartyData> {
		const partyCode = (new Date().getTime() % SIX_CHARS).toString(36);
		const partyDoc = this.db.doc<PartyData>(`parties/${partyCode}`);
		void partyDoc.set({
			level: "easy",
			participants: [],
		});
		return partyDoc;
	}

	getUserDataDoc(uuid: string): AngularFirestoreDocument<UserData> {
		return this.db.doc<UserData>(`users/${uuid}`);
	}

	getPartyDoc(partyCode: string): AngularFirestoreDocument<PartyData> {
		return this.db.doc<PartyData>(`parties/${partyCode}`);
	}

	joinParty(
		userDoc: AngularFirestoreDocument<UserData>,
		partyDoc: AngularFirestoreDocument<PartyData>,
		userInfo: firebase.UserInfo,
		userData: UserData,
		party: PartyData,
	): void {
		userData.partyCode = partyDoc.ref.id;
		void userDoc.update({ partyCode: userData.partyCode });

		const participants = party.participants;
		// user has already joined the party?
		if (participants.some((p) => p.uid === userInfo.uid)) return;

		participants.push({
			displayName: assertDefined(userInfo.displayName),
			photoURL: userInfo.photoURL,
			uid: userInfo.uid,
		});
		void partyDoc.update({ participants: participants });
	}

	abandonParty(
		userDoc: AngularFirestoreDocument<UserData>,
		partyDoc: AngularFirestoreDocument<PartyData>,
		uid: string,
		party: PartyData,
	): void {
		void userDoc.update({
			partyCode: (firebase.firestore.FieldValue.delete() as unknown) as string,
		});
		const participants = party.participants;
		for (let i = 0; i < participants.length; i++)
			if (participants[i].uid === uid) {
				participants.splice(i, i + 1);
				break;
			}
		if (participants.length === 0) void partyDoc.delete();
		else void partyDoc.update(party);
	}

	async getData<T>(doc: AngularFirestoreDocument<T>): Promise<T | undefined> {
		return doc.valueChanges().pipe(first()).toPromise();
	}

	valueChangesWithoutOwn<T>(
		doc: AngularFirestoreDocument<T> | undefined,
	): Observable<T | undefined> {
		if (doc === undefined) return of(undefined);
		return doc.snapshotChanges().pipe(
			// ignore all snapshots which are created from myself
			filter((snapshot) => !snapshot.payload.metadata.hasPendingWrites),
			map((snapshot) => snapshot.payload.data()),
		);
	}
}
