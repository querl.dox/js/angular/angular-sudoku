import { demoGames, solvedDemoGames } from "app/services/demo.games";
import { Position } from "app/services/position";
import { SolvedField } from "app/services/solved-field";
import { copyAndUnshift, SudokuService } from "app/services/sudoku.service";

//tslint:disable:no-floating-promises

describe("SudokuService", () => {
	let service: SudokuService;
	beforeEach(() => (service = new SudokuService()));

	it("should be created", () => {
		expect(service).toBeTruthy();
	});

	it("should solve next field and update demo game 1", () => {
		service.newGame();
		expect(service.solveNextAndUpdate()).toEqual(
			new SolvedField(new Position(7, 6), 4, "OnlyPossibleDigitForCell"),
		);
		const expected = copyAndUnshift(demoGames[0]);
		expected[7][6] = 4;
		expect(service.grid).toEqual(expected);
	});

	it("should solve all and update all demo games", () => {
		for (const solvedDemoGame of solvedDemoGames) {
			service.newGame();
			service.solveAllAndUpdate();
			expect(service.grid).toEqual(copyAndUnshift(solvedDemoGame));
		}
	});

	it("should return correct answer, if digit is possible", () => {
		service.newGame();
		expect(service.isDigitPossible(new Position(1, 1), 1)).toBeTruthy();
		expect(service.isDigitPossible(new Position(9, 9), 1)).toBeFalsy();
	});

	it("should return correct answer, if digit is correct", () => {
		service.newGame();
		expect(service.isDigitCorrect(new Position(1, 1), 5)).toBeTruthy();
		expect(service.isDigitCorrect(new Position(1, 1), 1)).toBeFalsy();
		service.clearGame();
		expect(service.isDigitCorrect(new Position(1, 1), 1)).toBeTruthy();
	});
});
