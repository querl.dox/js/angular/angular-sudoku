import { Injectable } from "@angular/core";
import { GameData, Level } from "app/server/data";
import {
	demoGames,
	getLevelFactor,
	levels,
	solvedDemoGames,
} from "app/services/demo.games";
import { generateOneToNine } from "app/services/digit";
import {
	allPositions,
	allPositionsInRegion,
	PosIter,
	Position,
	Region,
} from "app/services/position";
import {
	FieldState,
	SetDigitMessage,
	SetDigitResult,
	SetDigitResultKind,
} from "app/services/set-digit-result";
import { SolvedField } from "app/services/solved-field";
import { Sudoku } from "app/services/sudoku";
import { nullToUndefined, undefinedToNull } from "app/utils/util";

export function copyAndUnshift(
	src: OneToNineOrUndefined[][],
): OneToNineOrUndefined[][] {
	const result: OneToNineOrUndefined[][] = [];
	for (let i = 0; i < src.length; i++) {
		result[i + 1] = src[i].slice();
		result[i + 1].unshift(undefined);
	}
	return result;
}

const MAX_POINTS_TO_WIN_FOR_EASY_LEVEL = 9;

@Injectable()
export class SudokuService {
	private gameIndex = -1;
	grid: OneToNineOrUndefined[][] = [[], [], [], [], [], [], [], [], [], []];
	score = 0;
	private startTime = 0;
	constructor() {
		for (const pos of allPositions()) this.setDigit(pos);
	}

	getPointsToWin(): number {
		const elapsedSeconds = Math.floor(
			(new Date().getTime() - this.startTime) / 1000,
		);
		return Math.max(1, this.getMaxPointsToWin() - elapsedSeconds);
	}

	private getMaxPointsToWin(): number {
		return MAX_POINTS_TO_WIN_FOR_EASY_LEVEL * getLevelFactor(this.getLevel());
	}

	isStarted(): boolean {
		for (const pos of allPositions())
			if (this.getDigit(pos) !== undefined && !this.isInitialClue(pos))
				return true;
		return false;
	}

	getLevel(): Level {
		return levels[this.gameIndex];
	}

	isUserDefined(): boolean {
		return this.gameIndex === -1;
	}

	isSolved(): boolean {
		for (const pos of allPositions())
			if (this.getDigit(pos) === undefined) return false;
		return true;
	}

	newGame(level?: Level): Position {
		this.startTimer();
		do this.gameIndex = (this.gameIndex + 1) % demoGames.length;
		while (level !== undefined && levels[this.gameIndex] !== level);
		const demoGame = demoGames[this.gameIndex];

		this.grid = copyAndUnshift(demoGame);

		for (const pos of allPositions())
			if (!this.isInitialClue(pos)) return new Position(pos.row, pos.col);
		throw new Error("no free position, all fields are initial clues!");
	}

	clearGame(): void {
		this.gameIndex = -1;
		for (const pos of allPositions()) this.setDigit(pos);
	}

	solveNextAndUpdate(): SolvedField | undefined {
		const s = new Sudoku(this.grid);
		const solvedField = s.solveNext();
		if (solvedField !== undefined) {
			this.setDigit(solvedField.position, solvedField.digit);
			if (!this.isUserDefined()) this.score -= 2 * this.getPointsToWin();
		}
		this.startTimer();
		return solvedField;
	}

	solveAllAndUpdate(): void {
		const s = new Sudoku(this.grid);
		s.solveAll();
		for (const pos of allPositions()) this.setDigit(pos, s.getField(pos).digit);
	}

	private startTimer(): void {
		this.startTime = new Date().getTime();
	}

	getDigit(position: Position): OneToNineOrUndefined {
		return this.grid[position.row][position.col];
	}

	setDigitChecked(
		position: Position,
		digit?: OneToNine,
	): SetDigitResult | undefined {
		if (digit === undefined) {
			this.setDigit(position);
			return undefined;
		}

		const result = this.setDefinedDigitChecked(position, digit);
		if (!this.isUserDefined()) this.score += result.points;
		this.startTimer();
		return result;
	}

	private setDefinedDigitChecked(
		position: Position,
		digit: OneToNine,
	): SetDigitResult {
		const oldDigit = this.getDigit(position);
		this.setDigit(position);

		const fieldStates: FieldState[][] = [
			[],
			[],
			[],
			[],
			[],
			[],
			[],
			[],
			[],
			[],
		];

		if (!this.isDigitPossible(position, digit)) {
			this.setDigit(position, oldDigit);
			fieldStates[position.row][position.col] = "warning";
			return new SetDigitResult(
				"warning",
				"digitNotPossible",
				fieldStates,
				-2 * this.getPointsToWin(),
			);
		}

		if (!this.isDigitCorrect(position, digit)) {
			this.setDigit(position, oldDigit);
			fieldStates[position.row][position.col] = "warning";
			return new SetDigitResult(
				"warning",
				"digitNotCorrect",
				fieldStates,
				-2 * this.getPointsToWin(),
			);
		}

		this.setDigit(position, digit);
		if (this.isSolved()) {
			for (const pos of allPositions())
				fieldStates[pos.row][pos.col] = "solvedSudoku";
			fieldStates[position.row][position.col] = "solvedFieldInSolvedSudoku";
			return new SetDigitResult(
				"solved",
				"solved",
				fieldStates,
				this.getPointsToWin(),
				2 * this.getMaxPointsToWin(),
			);
		}

		let kind: SetDigitResultKind;
		let message: SetDigitMessage;
		if (this.getExhaustedDigits()[digit]) {
			for (const pos of allPositions())
				if (this.getDigit(pos) === digit)
					fieldStates[pos.row][pos.col] = "solvedGroup";
			fieldStates[position.row][position.col] = "solvedFieldInSolvedGroup";
			kind = "solved";
			message = "solvedDigit";
		}

		this.setFieldStatesForSolvedRegion(position, "Column", fieldStates);
		this.setFieldStatesForSolvedRegion(position, "Row", fieldStates);
		this.setFieldStatesForSolvedRegion(position, "Box", fieldStates);

		if (fieldStates[position.row][position.col] === undefined)
			fieldStates[position.row][position.col] = "solvedField";

		const bonusPoints = kind === "solved" ? this.getMaxPointsToWin() : 0;
		return new SetDigitResult(
			kind,
			message,
			fieldStates,
			this.getPointsToWin(),
			bonusPoints,
		);
	}

	private setFieldStatesForSolvedRegion(
		position: Position,
		region: Region,
		fieldStates: FieldState[][],
	): void {
		if (this.isRegionSolved(allPositionsInRegion(position, region))) {
			for (const pos of allPositionsInRegion(position, region))
				fieldStates[pos.row][pos.col] = "solvedGroup";
			fieldStates[position.row][position.col] = "solvedFieldInSolvedGroup";
		}
	}

	private isRegionSolved(posIter: PosIter): boolean {
		for (const p of posIter) if (this.getDigit(p) === undefined) return false;
		return true;
	}

	private setDigit(position: Position, digit?: OneToNine): void {
		this.grid[position.row][position.col] = digit;
	}

	getExhaustedDigits(): boolean[] {
		const occurrences: number[] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
		for (const pos of allPositions()) {
			const digit = this.getDigit(pos);
			if (digit !== undefined) occurrences[digit]++;
		}
		const result: boolean[] = [];
		for (const i of generateOneToNine()) result[i] = occurrences[i] === 9;
		return result;
	}

	getGridForOnlyOnePossibleDigit(): boolean[][] {
		const s = new Sudoku(this.grid);
		const result: boolean[][] = [[], [], [], [], [], [], [], [], [], []];
		for (const pos of allPositions())
			result[pos.row][pos.col] =
				s.getField(pos).getOnlyPossibleDigit() !== undefined;
		return result;
	}

	isDigitPossible(position: Position, digit: OneToNine): boolean {
		const s = new Sudoku(this.grid);
		return s.getField(position).isDigitPossible(digit);
	}

	isDigitCorrect(position: Position, digit: OneToNine): boolean {
		return (
			this.isUserDefined() ||
			solvedDemoGames[this.gameIndex][position.row - 1][position.col - 1] ===
				digit
		);
	}

	isInitialClue(position: Position): boolean {
		return (
			!this.isUserDefined() &&
			demoGames[this.gameIndex][position.row - 1][position.col - 1] !==
				undefined
		);
	}

	setGameData(gameData: GameData): void {
		if (
			gameData.gameIndex !== undefined &&
			this.gameIndex !== gameData.gameIndex
		)
			this.setGameIndex(gameData.gameIndex);

		if (gameData.digits !== undefined) this.setDigits(gameData.digits);

		if (gameData.score !== undefined) this.score = gameData.score;
	}

	setGameIndex(gameIndex: number): void {
		this.gameIndex = gameIndex;
		if (this.isUserDefined())
			for (const pos of allPositions()) this.setDigit(pos);
		else this.grid = copyAndUnshift(demoGames[this.gameIndex]);
	}

	setDigits(digits: OneToNineOrNull[]): void {
		let i = 0;
		for (const pos of allPositions())
			if (!this.isInitialClue(pos)) {
				const digit = digits[i++];
				// only set a digit if it is defined (i.e. never remove a valid digit in a multi-player race condition)
				// or if it is a user defined game
				if (digit !== null || this.isUserDefined())
					this.setDigit(pos, nullToUndefined(digit));
			}
	}

	getGameData(): GameData {
		return {
			digits: this.getGridValuesAsArray(),
			gameIndex: this.gameIndex,
			score: this.score,
		};
	}

	private getGridValuesAsArray(): OneToNineOrNull[] {
		let i = 0;
		const result: OneToNineOrNull[] = [];
		for (const pos of allPositions())
			if (!this.isInitialClue(pos))
				result[i++] = undefinedToNull(
					this.getDigit(new Position(pos.row, pos.col)),
				);
		return result;
	}
}
