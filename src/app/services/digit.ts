export function* generateOneToNine(): IterableIterator<OneToNine> {
	for (let i = 1; i <= 9; i++) yield i as OneToNine;
}

export function* generateOneFourSeven(): IterableIterator<OneFourSeven> {
	yield 1;
	yield 4;
	yield 7;
}

export function nextLowerOfOneFourSeven(oneToNine: OneToNine): OneFourSeven {
	return (Math.floor((oneToNine - 1) / 3) * 3 + 1) as OneFourSeven;
}

export function add2ToOneFourSeven(oneFourSeven: OneFourSeven): OneToNine {
	return (oneFourSeven + 2) as OneToNine;
}
