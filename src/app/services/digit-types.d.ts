type OneFourSeven = 1 | 4 | 7;
type OneToNine = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9;
type OneToNineOrUndefined = OneToNine | undefined;
type OneToNineOrNull = OneToNine | null;
