import { Component } from "@angular/core";

export interface DigitCssClass {
	selectedDigit: boolean;
	exhaustedDigit: boolean;
}

export abstract class DigitController {
	abstract readonly digitCssClasses: DigitCssClass[];
	abstract readonly isUserDefined: boolean;
	abstract readonly reason: string;
	abstract readonly oneToNine: OneToNine[];

	abstract digitClicked(value: OneToNine | 0): void;
}

@Component({
	selector: "sudoku-digit",
	styleUrls: ["./digit.component.scss"],
	templateUrl: "./digit.component.html",
})
export class DigitComponent {
	constructor(public c: DigitController) {}
}
