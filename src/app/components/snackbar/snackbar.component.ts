import { Component, Inject, OnDestroy } from "@angular/core";
import {
	MatSnackBarRef,
	MAT_SNACK_BAR_DATA,
} from "@angular/material/snack-bar";
import { fromEvent, Subscription } from "rxjs";

@Component({
	templateUrl: "./snackbar.component.html",
})
export class SnackbarComponent implements OnDestroy {
	private subscription?: Subscription;

	constructor(
		public snackBarRef: MatSnackBarRef<SnackbarComponent>,
		@Inject(MAT_SNACK_BAR_DATA) public data: string,
	) {
		// Use setTimeout to avoid propagation of possible click event which opened this snackbar.
		// Because otherwise this event would immediately dismiss the snackbar.
		setTimeout(() => {
			this.subscription = fromEvent(document, "click").subscribe(() =>
				this.snackBarRef.dismiss(),
			);
		});
	}

	ngOnDestroy(): void {
		if (this.subscription !== undefined) this.subscription.unsubscribe();
	}
}
