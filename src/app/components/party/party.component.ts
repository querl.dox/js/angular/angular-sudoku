import { Component, Inject, OnDestroy } from "@angular/core";
import { FormControl, Validators } from "@angular/forms";
import {
	MatDialog,
	MatDialogRef,
	MAT_DIALOG_DATA,
} from "@angular/material/dialog";
import { State } from "app/server/state";
import { Subscription } from "rxjs";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";

export async function openPartyDialog(
	partyCode: string,
	dialog: MatDialog,
): Promise<boolean | undefined> {
	return dialog
		.open<PartyComponent, string, boolean>(PartyComponent, { data: partyCode })
		.afterClosed()
		.toPromise();
}

@Component({
	templateUrl: "./party.component.html",
})
export class PartyComponent implements OnDestroy {
	partyCodeFormControl = new FormControl("", [Validators.required]);

	private readonly subscription: Subscription;

	constructor(
		public dialogRef: MatDialogRef<PartyComponent, boolean>,
		@Inject(MAT_DIALOG_DATA) partyCode: string,
		private readonly state: State,
	) {
		this.subscription = this.partyCodeFormControl.valueChanges
			.pipe(debounceTime(300), distinctUntilChanged())
			.subscribe((v) => this.codeChange(v));
		this.partyCodeFormControl.setValue(partyCode);
	}

	ngOnDestroy(): void {
		this.subscription.unsubscribe();
	}

	newParty(): void {
		this.state.createAndJoinParty();
		this.dialogRef.close(true);
	}

	async codeChange(code: string): Promise<void> {
		if (await this.state.tryToJoinParty(code)) this.dialogRef.close(true);
		else this.partyCodeFormControl.setErrors({ invalid: true });
	}
}
