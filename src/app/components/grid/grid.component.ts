import { animate, keyframes, transition, trigger } from "@angular/animations";
import { Component } from "@angular/core";
import { FieldState } from "app/services/set-digit-result";
import {
	pulse,
	pulseThreeTimes,
	shake,
	zoomIn,
	zoomInAndPulse,
	zoomInAndPulseThreeTimes,
} from "app/utils/keyframes";

export interface FieldCssClass {
	initialClue: boolean;
	selectedPosition: boolean;
	lastSolvedField: boolean;
	groupForLastSolvedField: boolean;
	selectedDigit: boolean;
	onlyOnePossibleDigit: boolean;
}

export abstract class GridController {
	abstract readonly isUserDefined: boolean;
	abstract readonly grid: OneToNineOrUndefined[][];
	abstract readonly fieldCssClasses: FieldCssClass[][];
	abstract readonly fieldStates: FieldState[][];
	abstract readonly oneToNine: OneToNine[];

	abstract fieldClicked(row: OneToNine, col: OneToNine): void;
}

@Component({
	animations: [
		trigger("fieldAnimator", [
			transition("* => warning", animate(1000, keyframes(shake))),
			transition("* => solvedField", animate(500, keyframes(zoomIn))),
			transition(
				"* => solvedFieldInSolvedGroup",
				animate(1000, keyframes(zoomInAndPulse)),
			),
			transition("* => solvedGroup", animate("500ms 500ms", keyframes(pulse))),
			transition(
				"* => solvedFieldInSolvedSudoku",
				animate(2000, keyframes(zoomInAndPulseThreeTimes)),
			),
			transition(
				"* => solvedSudoku",
				animate("1500ms 500ms", keyframes(pulseThreeTimes)),
			),
		]),
	],
	selector: "sudoku-grid",
	styleUrls: ["./grid.component.scss"],
	templateUrl: "./grid.component.html",
})
export class GridComponent {
	constructor(public c: GridController) {}
}
