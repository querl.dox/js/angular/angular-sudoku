import { Component } from "@angular/core";
import { MatDialog, MatDialogRef } from "@angular/material/dialog";
import { TranslateService } from "@ngx-translate/core";
import { Level, PartyData } from "app/server/data";
import { State } from "app/server/state";
import { assertDefined, share } from "app/utils/util";

export function openPartyListDialog(dialog: MatDialog): void {
	void dialog.open<PartyListComponent, void, void>(PartyListComponent, {
		hasBackdrop: false,
	});
}

@Component({
	styleUrls: ["./partylist.component.scss"],
	templateUrl: "./partylist.component.html",
})
export class PartyListComponent {
	constructor(
		public dialogRef: MatDialogRef<PartyListComponent, boolean>,
		private readonly ts: TranslateService,
		private readonly state: State,
	) {
		dialogRef.afterClosed().subscribe((result) => {
			if (result === undefined) this.state.abandonParty();
		});
	}

	get partyCode(): string {
		return assertDefined(this.state.userData!.partyCode);
	}

	get partyData(): PartyData {
		return assertDefined(this.state.partyData);
	}

	get firstParticipant(): boolean {
		return this.state.firstPartyParticipant;
	}

	levelChange(level: Level): void {
		this.partyData.level = level;
		this.state.savePartyData();
	}

	invite(): void {
		share(
			this.ts.instant("party.invitation", this.state.userInfo),
			this.ts.instant("party.invitation", this.state.userInfo),
			"https://winni.gitlab.io/angular-sudoku/?party=" + this.partyCode,
		);
	}

	play(): void {
		this.dialogRef.close(true);
	}
}
