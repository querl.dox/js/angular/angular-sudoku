import { Component, Inject } from "@angular/core";
import {
	MatDialog,
	MatDialogRef,
	MAT_DIALOG_DATA,
} from "@angular/material/dialog";

export interface ConfirmDialogKeys {
	title: string;
	question: string;
	confirmButton?: string;
}

@Component({
	templateUrl: "./confirm.component.html",
})
export class ConfirmComponent {
	constructor(
		public dialogRef: MatDialogRef<ConfirmComponent>,
		@Inject(MAT_DIALOG_DATA) readonly keys: ConfirmDialogKeys,
	) {}
}

export function openConfirmDialog(
	skipDialog: boolean,
	keys: ConfirmDialogKeys,
	confirmHandler: () => void,
	dialog: MatDialog,
): void {
	if (skipDialog) confirmHandler();
	else
		dialog
			.open<ConfirmComponent, ConfirmDialogKeys, boolean>(ConfirmComponent, {
				data: keys,
				hasBackdrop: false,
			})
			.afterClosed()
			.subscribe((result = false) => {
				if (result) confirmHandler();
			});
}
