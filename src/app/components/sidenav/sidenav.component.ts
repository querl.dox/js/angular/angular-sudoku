import { Component } from "@angular/core";
import { Level } from "app/server/data";
import { UserInfo } from "firebase";

export interface Theme {
	name: string;
	primaryColor: string;
	accentColor: string;
	backgroundColor: string;
}

export abstract class SidenavController {
	abstract readonly currentTheme: string;
	abstract readonly themes: Theme[];
	abstract readonly userInfo?: UserInfo;
	abstract readonly partyStarted: boolean;

	abstract newGame(level: Level): void;
	abstract joinParty(): void;
	abstract abandonParty(): void;
	abstract ownGame(): void;

	abstract solveNext(): void;
	abstract solveAll(): void;
	abstract isSolved(): boolean;

	abstract setTheme(theme: string): void;

	abstract login(): void;
	abstract logout(): void;
	abstract share(): void;
	abstract about(): void;
}

@Component({
	selector: "sudoku-sidenav",
	styleUrls: ["./sidenav.component.scss"],
	templateUrl: "./sidenav.component.html",
})
export class SidenavComponent {
	constructor(public c: SidenavController) {}
}
