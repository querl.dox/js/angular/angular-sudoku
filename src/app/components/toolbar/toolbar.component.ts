import { Component } from "@angular/core";
import { MatSidenav } from "@angular/material/sidenav";
import { Level } from "app/server/data";
import { UserInfo } from "firebase";

export abstract class ToolbarController {
	abstract readonly sidenav: MatSidenav;
	abstract readonly isUserDefined: boolean;
	abstract readonly score: number;
	abstract readonly pointsToWin: number;
	abstract readonly level: Level;
	abstract readonly userInfo?: UserInfo;

	abstract login(): void;
	abstract isSolved(): boolean;
}

@Component({
	selector: "sudoku-toolbar",
	styleUrls: ["./toolbar.component.scss"],
	templateUrl: "./toolbar.component.html",
})
export class ToolbarComponent {
	constructor(public c: ToolbarController) {}
}
