export function assertNever(x: never): never {
	throw new Error(`Unexpected object: ${x}`);
}

export function nullToUndefined<T>(value: T | null): T | undefined {
	return value === null ? undefined : value;
}

export function undefinedToNull<T>(value: T | undefined): T | null {
	// tslint:disable-next-line:no-null-keyword
	return value === undefined ? null : value;
}

// tslint:disable-next-line: no-null-undefined-union
export function assertDefined<T>(x: T | null | undefined): T {
	if (x === null) throw new Error("unexpected null value");
	if (x === undefined) throw new Error("unexpected undefined value");
	return x;
}

export function hasProperty<K extends string>(
	unknownValue: unknown,
	propertyName: K,
): unknownValue is { [_ in K]: unknown } {
	if (typeof unknownValue !== "object") return false;

	if (unknownValue === null) return false;

	return propertyName in unknownValue;
}

export function share(title: string, text: string, url: string): void {
	if (hasProperty(navigator, "share") && typeof navigator.share === "function")
		navigator.share({
			text: text,
			title: title,
			url: url,
		});
	else location.href = `mailto:?subject=${title}&body=${text}%0D%0A${url}`;
}
