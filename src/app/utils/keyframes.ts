import { style } from "@angular/animations";

const none = style({});

const right = style({ transform: "translate3d(5px, 0, 0)" });
const left = style({ transform: "translate3d(-5px, 0, 0)" });

const far = style({ opacity: 0, transform: "scale3d(0, 0, 0)" });
const near = style({ opacity: 0.5, transform: "scale3d(.5, .5, .5)" });
const here = style({ opacity: 1, transform: "scale3d(1, 1, 1)" });
const big = style({ opacity: 1, transform: "scale3d(1.5, 1.5, 1.5)" });

export const shake = [
	none,
	left,
	right,
	left,
	right,
	left,
	right,
	left,
	right,
	left,
	none,
];

export const zoomIn = [far, none];

export const zoomInAndPulse = [far, near, here, big, none];

export const zoomInAndPulseThreeTimes = [
	far,
	near,
	here,
	big,
	none,
	big,
	none,
	big,
	none,
];

export const pulse = [none, big, none];

export const pulseThreeTimes = [none, big, none, big, none, big, none];
