import { OverlayContainer } from "@angular/cdk/overlay";
import {
	Component,
	ElementRef,
	forwardRef,
	OnDestroy,
	Provider,
	Renderer2,
	ViewChild,
} from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { MatDialog } from "@angular/material/dialog";
import { MatSidenav } from "@angular/material/sidenav";
import {
	MatSnackBar,
	MatSnackBarConfig,
	MatSnackBarRef,
} from "@angular/material/snack-bar";
import { TranslateService } from "@ngx-translate/core";
import {
	ConfirmDialogKeys,
	openConfirmDialog,
} from "app/components/confirm/confirm.component";
import {
	DigitController,
	DigitCssClass,
} from "app/components/digit/digit.component";
import {
	FieldCssClass,
	GridController,
} from "app/components/grid/grid.component";
import { openLoginDialog } from "app/components/login/login.component";
import { openPartyDialog } from "app/components/party/party.component";
import { openPartyListDialog } from "app/components/partylist/partylist.component";
import {
	SidenavController,
	Theme,
} from "app/components/sidenav/sidenav.component";
import { SnackbarComponent } from "app/components/snackbar/snackbar.component";
import { ToolbarController } from "app/components/toolbar/toolbar.component";
import { Level } from "app/server/data";
import { State } from "app/server/state";
import { generateOneToNine } from "app/services/digit";
import { allPositions, Position } from "app/services/position";
import { FieldState } from "app/services/set-digit-result";
import { SolvedField } from "app/services/solved-field";
import { SudokuService } from "app/services/sudoku.service";
import { share } from "app/utils/util";
import { UserInfo } from "firebase";
import { Subscription } from "rxjs";

@Component({
	providers: [
		SudokuService,
		provideAppComponentAs(SidenavController),
		provideAppComponentAs(ToolbarController),
		provideAppComponentAs(GridController),
		provideAppComponentAs(DigitController),
	],
	selector: "sudoku-root",
	styleUrls: ["./app.component.scss"],
	templateUrl: "./app.component.html",
})
export class AppComponent
	implements
		SidenavController,
		ToolbarController,
		GridController,
		DigitController,
		OnDestroy {
	private selectedPosition: Position;
	private selectedPositionVisible = false;
	private selectedDigit?: OneToNine = 1;
	private lastSolvedField?: SolvedField;
	private readonly subs = new Subscription();

	@ViewChild(MatSidenav, { static: true }) readonly sidenav!: MatSidenav;

	currentTheme = "";
	reason = "";
	fieldCssClasses: FieldCssClass[][] = [];
	digitCssClasses: DigitCssClass[] = [];
	fieldStates: FieldState[][] = [];
	intervalID: NodeJS.Timeout;
	pointsToWin = 1;
	themes: Theme[] = [
		{
			accentColor: "#e91e63",
			backgroundColor: "#fafafa",
			name: "",
			primaryColor: "#3f51b5",
		},
		{
			accentColor: "#ffc107",
			backgroundColor: "#fafafa",
			name: "light-deeppurple-amber",
			primaryColor: "#673ab7",
		},
		{
			accentColor: "#4caf50",
			backgroundColor: "#303030",
			name: "dark-purple-green",
			primaryColor: "#9c27b0",
		},
		{
			accentColor: "#607d8b",
			backgroundColor: "#303030",
			name: "dark-pink-bluegrey",
			primaryColor: "#e91e63",
		},
	];

	oneToNine: OneToNine[] = [1, 2, 3, 4, 5, 6, 7, 8, 9];

	// tslint:disable-next-line
	constructor(
		private readonly ts: TranslateService,
		private readonly ss: SudokuService,
		private readonly snackBar: MatSnackBar,
		private readonly element: ElementRef,
		private readonly renderer: Renderer2,
		private readonly overlayContainer: OverlayContainer,
		private readonly dialog: MatDialog,
		private readonly state: State,
		private readonly auth: AngularFireAuth,
	) {
		// Workaround until fix for https://github.com/angular/angularfire2/issues/1347
		//    if (environment.production && "serviceWorker" in navigator)
		//      navigator.serviceWorker.getRegistration()
		//        .then(registration =>
		//          registration !== undefined ? registration : navigator.serviceWorker.register("./ngsw-worker.js"))
		//        .catch(e => { throw new Error(e); });

		ts.setDefaultLang("en");
		ts.addLangs(["de", "fr"]);
		ts.use(ts.getBrowserLang());

		for (const i of generateOneToNine()) {
			this.fieldCssClasses[i] = [];
			this.fieldStates[i] = [];
			this.grid[i] = [];
		}

		this.selectedPosition = this.ss.newGame("easy");
		this.update();

		this.subs.add(
			this.state.userData$.subscribe((userData) => {
				this.checkUrlParams();
				this.setThemeInternal(
					userData === undefined || userData.theme === undefined
						? ""
						: userData.theme,
				);
				ss.setGameData(
					userData === undefined || userData.gameData === undefined
						? { score: 0 }
						: userData.gameData,
				);
				this.update(this.lastSolvedField);
			}),
		);

		this.intervalID = setInterval(
			() => (this.pointsToWin = this.ss.getPointsToWin()),
			100,
		);
	}

	checkUrlParams(): void {
		const url = new URL(window.location.href);
		const partyCode = url.searchParams.get("party");
		if (partyCode !== null) {
			void this.joinParty(partyCode);
			window.history.replaceState(undefined, "", window.location.pathname);
		}
	}

	ngOnDestroy(): void {
		clearInterval(this.intervalID);
		this.subs.unsubscribe();
	}

	setTheme(theme: string): void {
		this.dismissAndClose();
		this.setThemeInternal(theme);
		this.saveUserData();
	}

	private setThemeInternal(theme: string): void {
		if (theme === "light")
			// tslint:disable-next-line:no-parameter-reassignment
			theme = "";
		if (this.currentTheme.length > 0) {
			this.renderer.removeClass(this.element.nativeElement, this.currentTheme);
			this.overlayContainer
				.getContainerElement()
				.classList.remove(this.currentTheme);
		}

		if (theme.length > 0) {
			this.renderer.addClass(this.element.nativeElement, theme);
			this.overlayContainer.getContainerElement().classList.add(theme);
		}
		this.currentTheme = theme;
	}

	get userInfo(): UserInfo | undefined {
		return this.state.userInfo;
	}

	login(): void {
		this.dismissAndClose();
		void openLoginDialog(this.dialog);
	}

	logout(): void {
		this.dismissAndClose();
		void this.auth.signOut();
	}

	share(): void {
		this.dismissAndClose();
		share(
			"Angular Sudoku",
			"Angular Sudoku",
			"https://winni.gitlab.io/angular-sudoku/",
		);
	}

	about(): void {
		this.dismissAndClose();
		window.open("https://gitlab.com/winni/angular-sudoku#angular-sudoku");
	}

	get grid(): OneToNineOrUndefined[][] {
		return this.ss.grid;
	}

	get level(): Level {
		return this.ss.getLevel();
	}

	get score(): number {
		return this.ss.score;
	}

	get isUserDefined(): boolean {
		return this.ss.isUserDefined();
	}

	newGame(level: Level): void {
		this.dismissAndClose();
		this.confirm(
			!this.ss.isStarted() || this.ss.isSolved(),
			{
				question: "confirmNewGame",
				title: "newGame",
			},
			() => {
				this.selectedPosition = this.ss.newGame(level);
				this.updateAndSave();
			},
		);
	}

	async joinParty(partyCode = ""): Promise<void> {
		this.dismissAndClose();
		if (this.state.userData === undefined) {
			// Wait before showing snackbar, otherwise it would be immediately closed.
			// This seems to happen, because of the browser's autofill functionality for input fields.
			// Even waiting for the dialog's "afterClosed" event does not help.
			setTimeout(() => this.openSnackBar("warning", "party.needsLogin"), 1000);
			if (await openLoginDialog(this.dialog))
				void this.openPartyDialog(partyCode);
		} else {
			this.saveUserData();
			void this.openPartyDialog(partyCode);
		}
	}

	abandonParty(): void {
		this.dismissAndClose();
		this.state.abandonParty();
	}

	private async openPartyDialog(partyCode: string): Promise<void> {
		if (await openPartyDialog(partyCode, this.dialog))
			openPartyListDialog(this.dialog);
	}

	get partyStarted(): boolean {
		return this.state.partyData !== undefined;
	}

	ownGame(): void {
		this.dismissAndClose();
		this.confirm(
			!this.ss.isStarted() || this.ss.isSolved(),
			{
				question: "confirmOwnGame",
				title: "ownGame",
			},
			() => {
				this.ss.clearGame();
				this.updateAndSave();
			},
		);
	}

	solveNext(): void {
		this.snackBar.dismiss();
		const solvedField = this.ss.solveNextAndUpdate();
		if (solvedField === undefined)
			this.openSnackBar("warning", "notYetSolvable");
		this.updateAndSave(solvedField);
	}

	solveAll(): void {
		this.dismissAndClose();
		this.confirm(
			!this.ss.isStarted(),
			{
				question: "confirmSolveAll",
				title: "solveAll",
			},
			() => {
				this.ss.solveAllAndUpdate();
				if (!this.ss.isSolved()) this.openSnackBar("warning", "notYetSolvable");
				this.updateAndSave();
			},
		);
	}

	isSolved(): boolean {
		return this.ss.isSolved();
	}

	keydown(keyboardEvent: KeyboardEvent): void {
		this.dismissAndClose();
		// Do not handle and do not prevent default, if a dialog is open.
		if (this.dialog.openDialogs.length > 0) return;
		this.selectedPositionVisible = true;
		this.handleKeyboardEvent(keyboardEvent);
	}

	fieldClicked(row: OneToNine, col: OneToNine): void {
		this.dismissAndClose();
		this.selectedPositionVisible = false;
		this.selectedPosition = new Position(row, col);
		this.setSelectedDigitInSelectedField();
		this.updateAndSave();
	}

	digitClicked(value: OneToNine | 0): void {
		this.dismissAndClose();
		// 0 is used here instead of undefined, because nuundefinedll can not be an index of the digitCssClasses array
		this.selectedDigit = value === 0 ? undefined : value;
		this.updateAndSave();
	}

	// tslint:disable-next-line:cognitive-complexity
	private handleKeyboardEvent(keyboardEvent: KeyboardEvent): void {
		const key = keyboardEvent.key;
		if (key >= "1" && key <= "9")
			this.setSelectedDigitOrSetDigitInSelectedField(+key as OneToNine);
		else
			switch (key) {
				case " ":
					this.setSelectedDigitOrSetDigitInSelectedField();
					break;
				case "ArrowUp":
					if (keyboardEvent.ctrlKey) this.selectedPosition.prevRowBox();
					else this.selectedPosition.prevRow();
					break;
				case "ArrowDown":
					if (keyboardEvent.ctrlKey) this.selectedPosition.nextRowBox();
					else this.selectedPosition.nextRow();
					break;
				case "ArrowLeft":
					if (keyboardEvent.ctrlKey) this.selectedPosition.prevColBox();
					else this.selectedPosition.prevCol();
					break;
				case "ArrowRight":
					if (keyboardEvent.ctrlKey) this.selectedPosition.nextColBox();
					else this.selectedPosition.nextCol();
					break;
				case "Home":
					if (keyboardEvent.ctrlKey) this.selectedPosition.start();
					else this.selectedPosition.col = 1;
					break;
				case "End":
					if (keyboardEvent.ctrlKey) this.selectedPosition.end();
					else this.selectedPosition.col = 9;
					break;
				case "Tab":
					if (keyboardEvent.shiftKey) this.selectedPosition.prev();
					else this.selectedPosition.next();
					break;
				case "Enter":
					this.setSelectedDigitInSelectedField();
					break;
				case "Delete":
				case "Backspace":
					this.setDigitInSelectedField();
					break;
				default:
					return; // Do not prevent default for an unhandled key.
			}
		this.updateAndSave();
		keyboardEvent.preventDefault();
	}

	private setSelectedDigitOrSetDigitInSelectedField(digit?: OneToNine): void {
		if (digit === this.selectedDigit) this.setSelectedDigitInSelectedField();
		else this.selectedDigit = digit;
	}

	private setSelectedDigitInSelectedField(): void {
		if (this.ss.isInitialClue(this.selectedPosition)) return;
		this.setDigitInSelectedField(this.selectedDigit);
	}

	private setDigitInSelectedField(digit?: OneToNine): void {
		const result = this.ss.setDigitChecked(this.selectedPosition, digit);
		if (result !== undefined) {
			if (result.kind !== undefined && result.message !== undefined)
				this.openSnackBar(result.kind, result.message, {
					bonusPoints: result.bonusPoints,
					digit: digit,
					negativePoints: -result.points,
					speedPoints: result.speedPoints,
				});
			this.fieldStates = result.fieldStates;
		}
	}

	private updateAndSave(solvedField?: SolvedField): void {
		this.update(solvedField);
		this.saveUserData();
	}

	private update(solvedField?: SolvedField): void {
		this.lastSolvedField = solvedField;
		if (solvedField !== undefined) {
			this.selectedPosition = solvedField.position;
			this.selectedDigit = solvedField.digit;
			this.reason = this.ts.instant(solvedField.reason, solvedField);
		} else this.reason = "";
		this.updateFieldCssClasses(solvedField);
		this.updateDigitCssClasses();
	}

	private updateFieldCssClasses(solvedField?: SolvedField): void {
		const gridForOnlyOnePossibleDigit: boolean[][] = this.ss.getGridForOnlyOnePossibleDigit();
		for (const pos of allPositions())
			this.fieldCssClasses[pos.row][pos.col] = {
				groupForLastSolvedField: false,
				initialClue: this.ss.isInitialClue(pos),
				lastSolvedField: false,
				onlyOnePossibleDigit: gridForOnlyOnePossibleDigit[pos.row][pos.col],
				selectedDigit:
					this.ss.getDigit(pos) === this.selectedDigit &&
					this.selectedDigit !== undefined,
				selectedPosition: false,
			};
		if (solvedField !== undefined) {
			this.fieldCssClasses[solvedField.position.row][
				solvedField.position.col
			].lastSolvedField = true;
			for (const p of solvedField.allPositionsForReason())
				this.fieldCssClasses[p.row][p.col].groupForLastSolvedField = true;
		}
		this.fieldCssClasses[this.selectedPosition.row][
			this.selectedPosition.col
		].selectedPosition = this.selectedPositionVisible;
	}

	private updateDigitCssClasses(): void {
		const exhaustedDigits = this.ss.getExhaustedDigits();
		for (const digit of generateOneToNine())
			this.digitCssClasses[digit] = {
				exhaustedDigit: exhaustedDigits[digit],
				selectedDigit: this.selectedDigit === digit,
			};
		this.digitCssClasses[0] = {
			exhaustedDigit: false,
			selectedDigit: this.selectedDigit === undefined,
		};
	}

	private confirm(
		skipDialog: boolean,
		keys: ConfirmDialogKeys,
		confirmHandler: () => void,
	): void {
		openConfirmDialog(skipDialog, keys, confirmHandler, this.dialog);
	}

	private openSnackBar(
		cssClass: string,
		message: string,
		params?: object,
	): MatSnackBarRef<SnackbarComponent> {
		const config = new MatSnackBarConfig();
		config.verticalPosition = "bottom";
		config.horizontalPosition = "center";
		config.duration = 5000;
		config.panelClass = [cssClass];
		config.data = this.ts.instant(message, params);
		return this.snackBar.openFromComponent(SnackbarComponent, config);
	}

	private dismissAndClose(): void {
		this.snackBar.dismiss();
		void this.sidenav.close();
	}

	private saveUserData(): void {
		const userData = this.state.userData;
		if (userData === undefined) return;
		userData.gameData = this.ss.getGameData();
		userData.theme = this.currentTheme;
		// tslint:disable-next-line: no-any no-null-keyword
		(userData.partyCode as any) = null;
		this.state.saveUserData();
	}
}

export function provideAppComponentAs(injectionToken: object): Provider {
	return {
		provide: injectionToken,
		useExisting: forwardRef(() => AppComponent),
	};
}
