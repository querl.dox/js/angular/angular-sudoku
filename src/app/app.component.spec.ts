/*
temporarily disabled because auf firebase
import { HttpClientModule } from "@angular/common/http";
import { async, TestBed } from "@angular/core/testing";
import { AngularFireModule } from "@angular/fire";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { FormsModule } from "@angular/forms";
import { MatButtonModule, MatDialogModule, MatIconModule, MatSidenavModule, MatSnackBarModule, MatToolbarModule } from "@angular/material";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MissingTranslationHandler, MissingTranslationHandlerParams, TranslateFakeLoader, TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { AppComponent } from "app/app.component";

export class MyMissingTranslationHandler implements MissingTranslationHandler {
  handle(params: MissingTranslationHandlerParams): string {
    return params.key + (params.interpolateParams ? ": " + JSON.stringify(params.interpolateParams) : "");
  }
}

describe("AppComponent", () => {
  beforeEach(async(async () => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
      ],
      imports: [
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        MatButtonModule,
        MatSidenavModule,
        MatToolbarModule,
        MatDialogModule,
        MatIconModule,
        MatSnackBarModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useClass: TranslateFakeLoader,
          },
          missingTranslationHandler: { provide: MissingTranslationHandler, useClass: MyMissingTranslationHandler },
        }),
        AngularFireModule.initializeApp({
          apiKey: "AIzaSyDLtCpl-B0yD4_Nr-ulcokswM9PKnK05IM",
          authDomain: "angular-sudoku.firebaseapp.com",
          databaseURL: "https://angular-sudoku.firebaseio.com",
          messagingSenderId: "995445029311",
          projectId: "angular-sudoku",
          storageBucket: "angular-sudoku.appspot.com",
        }),
        AngularFireAuthModule,
      ],
    })
      .compileComponents();
  }));

  it("should create the app", async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it("should render title in a h1 tag", async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector("h1").textContent).toContain("Sudoku");
  }));

  it("should have empty reason", async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.reason).toEqual("");
  }));

  it("should have defined reason after solve next", async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app: AppComponent = fixture.debugElement.componentInstance;
    app.solveNext();
    expect(app.reason)
      .toEqual(
        'OnlyPossibleDigitForCell:{"position":{"row":7,"col":6},"digit":4,"reason":"OnlyPossibleDigitForCell"}');
  }));
});
*/
