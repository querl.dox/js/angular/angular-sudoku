//tslint:disable
import { AngularSudokuPage } from "./app.po";

describe("angular-sudoku App", () => {
	let page: AngularSudokuPage;

	beforeEach(() => {
		page = new AngularSudokuPage();
	});

	it("should display welcome message", () => {
		page.navigateTo();
		expect(page.getParagraphText()).toEqual("Welcome to app!!");
	});
});
