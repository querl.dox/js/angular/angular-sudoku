import { createReadStream, createWriteStream, readdirSync, statSync } from "fs";
import { createGzip } from "zlib";

const extensions = [".css", "html", ".js", ".json", ".svg", ".txt", ".xml"];
function compress(path: string): void {
	if (statSync(path).isDirectory())
		readdirSync(path).forEach((child) => compress(`${path}/${child}`));
	else if (extensions.some((ext) => path.endsWith(ext)))
		createReadStream(path)
			.pipe(createGzip())
			.pipe(createWriteStream(path + ".gz"));
}

compress("dist");
